<?php $__env->startSection('content'); ?>



    <div class="content-wrapper">



        <!-- Content Header (Page header) -->



        <section class="content-header">



            <h1> <?php echo e(trans('labels.Variants')); ?> <small><?php echo e(trans('labels.ListingAllVariants')); ?>...</small> </h1>

            <ol class="breadcrumb">

                <li><a href="<?php echo e(URL::to('admin/dashboard/this_month')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('labels.breadcrumb_dashboard')); ?></a></li>

                <li><a href="<?php echo e(URL::to('admin/products')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('labels.ListingAllProducts')); ?></a></li>

                <li class="active"><?php echo e(trans('labels.AddVariants')); ?></li>

            </ol>

        </section>



        <!-- Main content -->

        <section class="content">



            <!-- Info boxes -->

            <!-- /.row -->

            <div class="row">



                <div class="col-md-12">

                    <div class="box">

                        <div class="box-header">

                            <h3 class="box-title"> <?php echo e(trans('labels.ListingAllVariants')); ?> </h3>

                            <div class="box-tools pull-right">

                                <a href="javascript:void(0)" onclick="addNewVariant()" type="button" class="btn btn-block btn-primary"><?php echo e(trans('labels.AddNewVariant')); ?></a>

                            </div>

                        </div>





                        <!-- /.box-header -->



                        <div class="box-body">

                            <div class="row">

                                <div class="col-xs-12">

                                    <?php if(count($errors) > 0): ?>

                                        <?php if($errors->any()): ?>

                                            <div class="alert alert-success alert-dismissible" role="alert">

                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                                <?php echo e($errors->first()); ?>


                                            </div>

                                        <?php endif; ?>

                                    <?php endif; ?>

                                </div>



                            </div>

                            <div class="row">

                                <div class="col-xs-12">
                                    <?php echo Form::open(array('id' => 'variant_form','url' =>'admin/addnewvariants', 'class' => 'form-horizontal form-validate')); ?>

                                    <?php echo Form::hidden('products_id',  $result['products_id'], array('class'=>'form-control', 'id'=>'products_id')); ?>

                                    <table id="example1" class="table table-bordered table-striped">

                                        <thead>

                                            <tr>

                                                <th colspan="4"><?php echo e(trans('labels.ManageVariants')); ?></th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>

                                    <div class="form-group">
                                        <button class="btn btn-success" id='submitBtn' style="float:right;margin-right: 10px;" disabled>Create Variants</button>
                                    </div>
                                    <?php echo Form::close(); ?>

                                    <div class="col-xs-12 text-right">





                                    </div>



                                </div>



                            </div>



                        </div>



                        <!-- /.box-body -->



                    </div>



                    <!-- /.box -->



                </div>



                <!-- /.col -->



            </div>







            <!-- /.row -->



            <!-- deleteAttributeModal -->



            <div class="modal fade" id="deleteattributeModal" tabindex="-1" role="dialog" aria-labelledby="deleteAttributeModalLabel">



                <div class="modal-dialog" role="document">



                    <div class="modal-content">



                        <div class="modal-header">



                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>



                            <h4 class="modal-title" id="deleteAttributeModalLabel"><?php echo e(trans('labels.DeleteOption')); ?></h4>



                        </div>



                        <?php echo Form::open(array('url' =>'admin/deleteattribute', 'name'=>'deleteAttribute', 'id'=>'deleteAttribute', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>




                        <?php echo Form::hidden('action',  'delete', array('class'=>'form-control')); ?>




                        <?php echo Form::hidden('option_id',  '', array('class'=>'form-control', 'id'=>'option_id')); ?>




                        <div class="modal-body">



                            <p><?php echo e(trans('labels.DeleteOptionPrompt')); ?></p>



                        </div>



                        <div class="modal-footer">



                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(trans('labels.Close')); ?></button>



                            <button type="submit" class="btn btn-primary" id="deleteAttribute"><?php echo e(trans('labels.DeleteOption')); ?></button>



                        </div>



                        <?php echo Form::close(); ?>




                    </div>



                </div>



            </div>







            <div class="modal fade" id="productListModal" tabindex="-1" role="dialog" aria-labelledby="productListModalLabel">



                <div class="modal-dialog" role="document">



                    <div class="modal-content">



                        <div class="modal-header">



                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>



                            <h4 class="modal-title" id="productListModalLabel"></h4>



                        </div>



                        <div class="modal-body">

                            <p><strong><?php echo e(trans('labels.DeletingErrorMessage')); ?></strong></p>

                            <ul style="padding:0" id="assciate-products">

                            </ul>

                        </div>



                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(trans('labels.Ok')); ?></button>

                        </div>



                    </div>



                </div>



            </div>



            <!-- Main row -->

            <!-- /.row -->

        </section>

        <!-- /.content -->



    </div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>