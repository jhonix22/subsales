<?php $__env->startSection('content'); ?>

<div class="content-wrapper"> 

  <!-- Content Header (Page header) -->

  <section class="content-header">

    <h1> <?php echo e(trans('labels.AddProduct')); ?> <small><?php echo e(trans('labels.AddProduct')); ?>...</small> </h1>

    <ol class="breadcrumb">

       <li><a href="<?php echo e(URL::to('admin/dashboard/this_month')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('labels.breadcrumb_dashboard')); ?></a></li>

      <li><a href="<?php echo e(URL::to('admin/products')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('labels.ListingAllProducts')); ?></a></li>

      <li class="active"><?php echo e(trans('labels.AddProduct')); ?></li>

    </ol>

  </section>

  

  <!-- Main content -->

  <section class="content"> 

    <!-- Info boxes --> 

    

    <!-- /.row -->



    <div class="row">

      <div class="col-md-12">

        <div class="box">

          <div class="box-header">

            <h3 class="box-title"><?php echo e(trans('labels.AddNewProduct')); ?> </h3>

          </div>

          

          <!-- /.box-header -->

          <div class="box-body">

            <div class="row">

              <div class="col-xs-12">

                    <div class="box box-info">

                        <!-- form start -->                        

                         <div class="box-body">

                          <?php if( count($errors) > 0): ?>

                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <div class="alert alert-danger" role="alert">

                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>

                                    <span class="sr-only"><?php echo e(trans('labels.Error')); ?>:</span>

                                    <?php echo e($error); ?>


                                </div>

                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                          <?php endif; ?>


                          <?php if(isset($result['success'])): ?>
                                  <div class="alert alert-success" role="alert">

                                      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>

                                      <span class="sr-only"><?php echo e(trans('labels.SuccessNotif')); ?>:</span>

                                      <?php echo e(trans('labels.SuccessNotif')); ?>

                                  </div>
                          <?php endif; ?>



                            <?php echo Form::open(array('url' =>'admin/addnewproduct', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')); ?>

                              <h4 for="name" class="col-sm-2 col-md-3 float-left"><?php echo e(trans('labels.Listing Details')); ?> </h4><div class="clearfix"></div>
                            	<div class="form-group">

                                  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.ProductTitle')); ?> </label>

                                  <div class="col-sm-10 col-md-4">
                                      <input  class="form-control" type="text" name="products_name" id="products_name" value="">
                                  </div>

                                </div>

                              <div class="form-group">

                                  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Product Type')); ?> </label>

                                  <div class="col-sm-10 col-md-4">
                                      <select name="product_type" id="product_type" class="form-control">
                                          <option value="1">Simple Product</option>
                                          <option value="2">Variable Product</option>
                                      </select>
                                  </div>

                              </div>

                              <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Subtitle')); ?>

                                      <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">

                                      (<?php echo e(trans('labels.AU')); ?>)</span></label>
                                  <div class="col-sm-10 col-md-4">
                                      <input  class="form-control" type="text" name="subtitle" id="subtitle" value="">

                                  </div>
                              </div>
                              <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Custom Label')); ?>

                                      <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">

                                      (<?php echo e(trans('labels.SKU')); ?>)</span>
                                  </label>
                                  <div class="col-sm-10 col-md-4">
                                      <input  class="form-control" type="text" name="custom_label" id="custom_label" value="">

                                  </div>
                              </div>

                                <div class="form-group">

                                  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Category')); ?></label>

                                  <div class="col-sm-10 col-md-4">

                                  <?php if(!empty(session('categories_id'))): ?>

										<?php 

                                        $cat_array = explode(',', session('categories_id'));                                        

                                         ?>

                                        <ul class="list-group list-group-root well">    

                                          <?php $__currentLoopData = $result['categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categories): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>   

                                          <?php if(in_array($categories->id,$cat_array)): ?>                                 

                                          <li href="#" class="list-group-item"><label style="width:100%"><input id="categories_<?=$categories->id?>" type="checkbox" class="required_one categories" name="categories[]" value="<?php echo e($categories->id); ?>" > <?php echo e($categories->name); ?></label></li>

                                          <?php endif; ?>

                                              <?php if(!empty($categories->sub_categories)): ?>

                                              <ul class="list-group">

                                                    	<li class="list-group-item" >

                                                    <?php $__currentLoopData = $categories->sub_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                    <?php if(in_array($sub_category->sub_id,$cat_array)): ?>  

                                                    <label><input type="checkbox" name="categories[]" class="required_one sub_categories sub_categories_<?=$categories->id?>" parents_id = '<?=$categories->id?>' value="<?php echo e($sub_category->sub_id); ?>"> <?php echo e($sub_category->sub_name); ?></label> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></li>

                                                    

                                              </ul>

                                              <?php endif; ?>

                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                          

                                        </ul>                                           

                                  <?php else: ?>

                                   <ul class="list-group list-group-root well">    

                                      <?php $__currentLoopData = $result['categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categories): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                    

                                      <li href="#" class="list-group-item"><label style="width:100%"><input id="categories_<?=$categories->id?>" type="checkbox" class="required_one categories" name="categories[]" value="<?php echo e($categories->id); ?>" > <?php echo e($categories->name); ?></label></li>

                                          <?php if(!empty($categories->sub_categories)): ?>

                                          <ul class="list-group">

                                                    <li class="list-group-item" >

                                                <?php $__currentLoopData = $categories->sub_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><label><input type="checkbox" name="categories[]" class="required_one sub_categories sub_categories_<?=$categories->id?>" parents_id = '<?=$categories->id?>' value="<?php echo e($sub_category->sub_id); ?>"> <?php echo e($sub_category->sub_name); ?></label><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></li>

                                                

                                          </ul>

                                          <?php endif; ?>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                          

                                    </ul>   

                                  <?php endif; ?>

                                                                               

                                      <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">

                                      <?php echo e(trans('labels.ChooseCatgoryText')); ?>.</span>

                                      <span class="help-block hidden"><?php echo e(trans('labels.textRequiredFieldMessage')); ?></span>

                                  </div>

                                </div>

                                                                

                                <div class="form-group removed_if_variant">

                                  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.UPC')); ?> </label>

                                  <div class="col-sm-10 col-md-4">
                                      <input type="text" name="upc" id="upc" class="form-control input">
                                  </div>

                                </div>

                              <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">
                                      <?php echo e(trans('labels.Condition')); ?>

                                  </label>
                                  <div class="col-sm-10 col-md-4">
                                      <select class="form-control field-validate" name="condition">

                                          <option value="0"><?php echo e(trans('labels.BrandNew')); ?></option>

                                          <option value="1"><?php echo e(trans('labels.OPV')); ?></option>

                                          <option value="2"><?php echo e(trans('labels.Mrefurbished')); ?></option>

                                          <option value="3"><?php echo e(trans('labels.Srefurbished')); ?></option>
                                          <option value="4"><?php echo e(trans('labels.Used')); ?></option>
                                          <option value="5"><?php echo e(trans('labels.ForPNW')); ?></option>

                                      </select>
                                  </div>
                              </div>

                              <style>
                                  #image_preview img{
                                      width: 100px;
                                      height: 80px;
                                  }
                                  #image_preview{
                                      display: flex;
                                      flex-flow: wrap;
                                  }
                                  #image_preview hr{
                                      width: 100%;
                                  }
                                  .btn-file input[type=file]{
                                      top: 0;
                                      right: 0;
                                      min-width: 100%;
                                      min-height: 100%;
                                      text-align: right;
                                      opacity: 0;
                                      background: none repeat scroll 0 0 transparent;
                                      cursor: inherit;
                                      display: block;
                                  }
                              </style>

                              <div class="form-group removed_if_variant">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">
                                      <?php echo e(trans('labels.Photos')); ?>

                                      <span id="photo_num">
                                          (<?=$result['photo_num']?>)
                                      </span>
                                  </label>
                                  <div class="col-sm-10 col-md-9">
                                      <span class="help-block" style="font-weight:normal;font-size:11px;">We recommend adding 3 more photos (Max 12) |
                                          <a class="text-danger" style="font-size:12px;" href="javasript:viod(0)" onclick="removeAllImages()">Remove all</a></span>
                                      <div tabindex="500" class="btn btn-primary btn-file">
                                          <i class="fa fa-folder-open"></i>
                                          <span class="hidden-xs">Browse …</span>
                                          <input type="file" style="opacity:0;" id="products_image" name="products_image[]" onchange="preview_image()" multiple/>
                                      </div>
                                      <div tabindex="500" class="btn btn-success btn-file" data-target="#importFromWebModal" data-toggle="modal">
                                          <i class="fa fa-download"></i>
                                          <span class="hidden-xs">Import from Web…</span>
                                      </div>
                                      <div id="image_preview"></div>
                                  </div>
                              </div>

                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.specifics')); ?></label>

                                  <div class="col-sm-10 col-md-4">
                                      <table class="table-no-border" id="metas">
                                      </table>
                                      <a href="javascript:void(0)" data-target="#addNewMetaModal" data-toggle="modal" class="link">
                                          <i class="fa fa-plus-circle">&nbsp;Add your own item specific</i>
                                      </a>
                                  </div>

                                </div>

                              <?php $__currentLoopData = $result['languages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $languages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <div class="form-group">

                                	<label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Description')); ?></label>

                                    <div class="col-sm-10 col-md-8">

                                        <span class="help-block text-right">
                                            <a href="#" class="link" data-toggle="modal" data-target="#mobileFriendlyCheckerModal">Mobile Friendly Checker</a>
                                        </span>
                                    	<textarea id="editor<?=$languages->languages_id?>" name="products_description" class="form-control" rows="5"></textarea>

                                    	<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">

                                       <?php echo e(trans('labels.EnterProductDetailIn')); ?> <?php echo e($languages->name); ?></span>
                                      </div>

                                </div>

                                  <hr>
                                  <h4 for="name" class="col-sm-2 col-md-3 float-left"><?php echo e(trans('labels.Selling Details')); ?> </h4><div class="clearfix"></div>

                                <div class="form-group">

                                  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Format')); ?></label>

                                  <div class="col-sm-2 col-md-2">

                                      <select name="format_<?=$languages->languages_id?>" id="format_<?=$languages->languages_id?>" class="form-control">
                                          <option value="0">
                                              <?php echo e(trans('labels.FixedPrice')); ?>

                                          </option>
                                      </select>

                                  </div>

                                </div>

                                <div class="form-group removed_if_variant">

                                      <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Price_')); ?></label>

                                      <div class="col-sm-4 col-md-2">

                                          <input class="form-control" type="text" name="products_price" id="products_price">
                                      </div>
                                  </div>



                                  <div class="form-group removed_if_variant">

                                      <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Quantity_')); ?></label>

                                      <div class="col-sm-4 col-md-2">

                                          <input class="form-control" type="text" name="quantity" id="quantity">
                                          <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                              <?php echo e(trans('labels.NumericValueError')); ?>

                                          </span>
                                      </div>
                                  </div>

                                                                

                                <div class="form-group">

                                  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Payment Options')); ?> (<?php echo e(trans('labels.Paypal')); ?>)</label>

                                  <div class="col-sm-10 col-md-4">

                                  	<input type="email" class="form-control" name="paypal_email" id="paypal_email">

                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">

                                    <?php echo e(trans('labels.Paypal Email')); ?></span>

                                  </div>

                                </div>

                                <div class="form-group">

                                  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Return Options')); ?></label>

                                  <div class="col-sm-10 col-md-8">
                                          <span class="help-block"><input type="checkbox" class="control-input" name="return" id="return" value="1">
                                          <?php echo e(trans('labels.Domestic Returns')); ?></span>
                                        <span class="help-block">
                                            <?php echo e(trans('labels.Return Days')); ?>

                                        </span>
                                      <div class="col-md-2" style="padding-left: 0px;">
                                          <input type="text" id="days" name="days" placeholder="Days" class="form-control">
                                          <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                              <?php echo e(trans('labels.NumericValueError')); ?>

                                          </span>
                                      </div>

                                  </div>

                                </div>

                                  <hr>
                                  <h4 for="name" class="col-sm-2 col-md-3 float-left"><?php echo e(trans('labels.Postage Details')); ?> </h4><div class="clearfix"></div>

                                <div class="form-group">

                                  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Dynamic Domestic Postage')); ?></label>

                                  <div class="col-sm-10 col-md-8">
                                     <table class="table" id="servicesTable" style="border: none;">
                                         <thead>
                                         <tr>
                                             <td>Services</td>
                                             <td>Cost</td>
                                         </tr>
                                         </thead>
                                         <style>
                                             .table tr, .table td{
                                                 border: none !important;
                                                 padding: 0 !important;
                                             }
                                         </style>
                                         <tr id="service">
                                             <td>
                                                 <select name="postage[]" id="postage_0" class="field-validate postage form-control">
                                                     <option value=""><?php echo e(trans('labels.ChooseValue')); ?></option>
                                                     <?php $__currentLoopData = $result['postages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $postage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                         <option value="<?php echo e($postage->postage_id); ?>-<?php echo e($postage->service); ?>"><?php echo e($postage->service); ?></option>
                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                 </select>
                                             </td>
                                             <td>
                                                 <div class="col-md-4" style="padding-left: 0px;">
                                                     <input type="text" class="form-control" id="postage_cost_0" min="0" name="postage_cost[]" value="0.00">
                                                     <a href="#" class="link text-danger float-right" id="removeService"><i class="fa fa-minus 2x">&nbsp;Remove Service</i></a>
                                                 </div>
                                             </td>
                                             <td></td>
                                         </tr>
                                     </table>
                                      <a href="#" class="link float-right" id="addMoreService"><i class="fa fa-plus 2x">&nbsp;Add Service</i></a>
                                  </div>

                                </div>

                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                              <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Item location')); ?></label>
                                  <div class="col-sm-8 col-md-4">
                                      <span class="help-block" style="font-weight:normal;font-size:14px;margin-bottom:0px;">
                                          <?php echo e(trans('labels.Country_')); ?>

                                      </span>
                                      <select name="country" id="country" class="form-control">
                                            <?php $__currentLoopData = $result['countries']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                              <option value="<?php echo e($country->countries_id); ?>"><?php echo e($country->countries_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      </select>
                                      <span class="help-block" style="font-weight:normal;font-size:14px;margin-bottom:0px;">
                                          <?php echo e(trans('labels.Postcode_')); ?>

                                      </span>
                                      <input type="text" name="postcode" id="postcode" class="form-control">
                                      <span class="help-block" style="font-weight:normal;font-size:14px;margin-bottom:0px;">
                                          <?php echo e(trans('labels.Suburb_')); ?>

                                      </span>
                                      <input type="text" name="suburb" id="suburb" class="form-control">
                                  </div>
                              </div>

                                

                              <!-- /.box-body -->

                              <div class="box-footer text-center">

                                <button type="submit" class="btn btn-primary pull-right"  id="attribute-btn"><?php echo e(trans('labels.Add Atrributes')); ?> <i class="fa fa-angle-right 2x"></i></button>

								  

                                <button type="submit" class="btn btn-primary pull-right"  id="normal-btn" style="display: none"><?php echo e(trans('labels.addinventory')); ?> <i class="fa fa-angle-right 2x"></i></button>

                                <button type="submit" class="btn btn-primary pull-right"  id="variant-btn" style="display: none"><?php echo e(trans('labels.AddProductVariant')); ?> <i class="fa fa-angle-right 2x"></i></button>
                                <button type="submit" class="btn btn-primary pull-right"  id="external-btn" style="display: none"><?php echo e(trans('labels.AddProducts')); ?></button>

                              </div>

                              

                              <!-- /.box-footer -->

                            <?php echo Form::close(); ?>


                              


                                
                              <div class="modal fade" id="addNewMetaModal" tabindex="-1" role="dialog" aria-labelledby="addImagesModalLabel">
                                  <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title" id="addNewMetaModalLabel"><?php echo e(trans('labels.newmetatext')); ?></h4>
                                          </div>
                                          <?php echo Form::open(array('url' =>'admin/addNewMeta', 'name'=>'addnewMetaForm', 'id'=>'addnewMetaForm', 'method'=>'post', 'class' => 'form-horizontal')); ?>

                                          <div class="modal-body">

                                              <div class="form-group">
                                                  <div class="col-sm-8 col-md-8">
                                                      <input type="text" class="form-control" name="metakey[]" id="metaKey">
                                                      <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
							  <?php echo e(trans('labels.metaKeyHelp')); ?></span>
                                                      <br>
                                                  </div>
                                              </div>
                                              <div class="form-group">
                                                  <div class="col-sm-8 col-md-8 ">
                                                      <input type="text" class="form-control" name="metavalue[]" id="metaValue">
                                                      <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
							     <?php echo e(trans('labels.metaValueHelp')); ?>

							     </span>
                                                  </div>
                                              </div>
                                              <div class="alert alert-danger addError" style="display: none; margin-bottom: 0;" role="alert"><i class="icon fa fa-ban"></i> <?php echo e(trans('labels.ChooseImageText')); ?> </div>

                                          </div>
                                          <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(trans('labels.cancelBtn')); ?> </button>
                                              <button type="button" class="btn btn-primary" id="addMeta"><?php echo e(trans('labels.saveBtn')); ?></button>
                                          </div>
                                          <?php echo Form::close(); ?>

                                      </div>
                                  </div>
                              </div>

                                
                              <div class="modal fade" id="mobileFriendlyCheckerModal" tabindex="-1" role="dialog" aria-labelledby="mobileFriendlyCheckerModalLabel">
                                  <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title" id="mobileFriendlyCheckerModallLabel"><?php echo e(trans('labels.mobileCheckerTitle')); ?></h4>
                                          </div>
                                          <div class="modal-body">
                                          </div>
                                          <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(trans('labels.cancelBtn')); ?> </button>
                                              <button type="button" class="btn btn-primary" id="addMeta"><?php echo e(trans('labels.saveBtn')); ?></button>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              
                              <div class="modal fade" id="importFromWebModal" tabindex="-1" role="dialog" aria-labelledby="importFromWebModalLabel">
                                  <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title" id="mobileFriendlyCheckerModallLabel"><?php echo e(trans('labels.ImportFromweb')); ?></h4>
                                              <div class="form-group">
                                                  <div id="url_container">
                                                      <input type="text" id="url" class="urls form-control">
                                                      <br>
                                                  </div>

                                                  <span class="help-block" style="font-weight:normal;font-size:14px;margin-bottom:0px;">
                                                      <?php echo e(trans('labels.EnterURL')); ?>

                                                  </span>
                                                  <a href="javascript:void(0);" onclick="addMoreURL()" style="float:right;" slass="text-primary">
                                                        Add More
                                                  </a>
                                              </div>
                                          </div>
                                          <div class="modal-body">
                                          </div>
                                          <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(trans('labels.cancelBtn')); ?> </button>
                                              <button type="button" class="btn btn-primary" id="addUrls"><?php echo e(trans('labels.AddURL')); ?></button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                        </div>

                  </div>

              </div>

            </div>

            

          </div>

          <!-- /.box-body --> 

        </div>

        <!-- /.box --> 

      </div>

      <!-- /.col --> 

    </div>

    <!-- /.row --> 

    

    <!-- Main row --> 

    

    <!-- /.row --> 

  </section>

  <!-- /.content --> 

</div>

<script src="<?php echo asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>

<script type="text/javascript">

		$(function () {

			

			//for multiple languages

			<?php $__currentLoopData = $result['languages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $languages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

				// Replace the <textarea id="editor1"> with a CKEditor

				// instance, using default configuration.

				CKEDITOR.replace('editor<?php echo e($languages->languages_id); ?>');

			

			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

			

			//bootstrap WYSIHTML5 - text editor

			$(".textarea").wysihtml5();

			

    });

</script>

<?php $__env->stopSection(); ?> 
<?php echo $__env->make('admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>