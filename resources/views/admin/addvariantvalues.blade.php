@extends('admin.layout')



@section('content')



<div class="content-wrapper">



    <!-- Content Header (Page header) -->



    <section class="content-header">



        <h1> {{ trans('labels.Variants') }} <small>{{ trans('labels.ListingAllVariants') }}...</small> </h1>

        <ol class="breadcrumb">

            <li><a href="{{ URL::to('admin/dashboard/this_month') }}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>

            <li><a href="{{ URL::to('admin/products')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.ListingAllProducts') }}</a></li>

            <li class="active">{{ trans('labels.AddVariants') }}</li>

        </ol>

    </section>



    <!-- Main content -->

    <section class="content">



        <!-- Info boxes -->

        <!-- /.row -->

        <div class="row">



            <div class="col-md-12">

                <div class="box">

                    <div class="box-body">

                        <div class="row">

                            <div class="col-xs-12">

                                @if (count($errors) > 0)

                                @if($errors->any())

                                <div class="alert alert-success alert-dismissible" role="alert">

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    {{$errors->first()}}

                                </div>

                                @endif

                                @endif

                            </div>



                        </div>

                        <div class="row">

                            <div class="col-xs-12">
                                {!! Form::open(array('url' =>'admin/addnewvariantvalues', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                                {!! Form::hidden('products_id',  $result['products_id'], array('class'=>'form-control', 'id'=>'products_id')) !!}
                                <table id="example1" class="table table-bordered table-striped">

                                    <thead>

                                    <tr>
                                        <th>#</th>
                                        <th>Variant Name</th>
                                        <th>Variant Code</th>
                                        <th>SKU</th>
                                        <th>UPC</th>
                                        <th>Model</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Photos</th>
                                        <th>X</th>
                                    </tr>

                                    </thead>

                                    <tbody>
                                        @foreach($result['values'] as $value)
                                            <input type="hidden" name="ids[]" value="{{$value->products_variants_values_id}}">
                                            <tr>
                                                <td>{{ $loop->index+1 }}</td>
                                                <td style="width:200px;"><input type="text" class="form-control" name="value_name[]" value="{{$value->value_name}}"></td>
                                                <td style="width:200px;"><input type="text" class="form-control" name="value_code[]" value="{{$value->value_code}}"></td>
                                                <td style="width:200px;">
                                                    <input type="text" class="form-control" name="sku[]" value="{{$value->sku}}">
                                                </td>
                                                <td style="width:200px;">
                                                    <input type="text" class="form-control" name="upc[]" value="{{$value->upc}}">
                                                </td>
                                                <td style="width:200px;">
                                                    <input type="text" class="form-control" name="model[]" value="{{$value->model}}">
                                                </td>
                                                <td style="width:90px;">
                                                    <input type="text" class="form-control" name="sell_price[]" value="{{$value->sell_price}}">
                                                </td>
                                                <td style="width:90px;">
                                                    <input type="text" class="form-control" name="quantity[]" value="{{$value->quantity}}">
                                                </td>
                                                <td><button class="btn btn-success openPhotoModal" data-id="{{$value->products_variants_values_id}}"><i class="fa fa-photo"></i></button></td>
                                                <td><button class="btn btn-danger" onclick="deleteVariantValue({{$value->products_variants_values_id}}); return false;">X</button></td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                                </table>

                                <div class="form-group">
                                    <button class="btn btn-primary" style="float:right;margin-right: 10px;">Save</button>
                                </div>

                                <div class="col-xs-12 text-right">





                                </div>

                                {!! Form::close() !!}

                            </div>



                        </div>



                    </div>



                    <!-- /.box-body -->



                </div>



                <!-- /.box -->



            </div>



            <!-- /.col -->



        </div>







        <!-- /.row -->



        <!-- addPhotoModal -->


        <style>
            #image_preview img{
                width: 100px;
                height: 80px;
            }
            #image_preview{
                display: flex;
                flex-flow: wrap;
            }
            #image_preview hr{
                width: 100%;
            }
            .btn-file input[type=file]{
                top: 0;
                right: 0;
                min-width: 100%;
                min-height: 100%;
                text-align: right;
                opacity: 0;
                background: none repeat scroll 0 0 transparent;
                cursor: inherit;
                display: block;
            }
        </style>



        <div class="modal fade" id="addPhotoModal" tabindex="-1" role="dialog" aria-labelledby="addPhotoModalLabel">



            <div class="modal-dialog" role="document">



                <div class="modal-content">



                    <div class="modal-header">



                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>



                        <h4 class="modal-title" id="deleteAttributeModalLabel">{{ trans('labels.AddImages') }}</h4>



                    </div>

                    <div class="previews">

                    </div>

                    {!! Form::open(array('url' =>'admin/addimages', 'name'=>'addimages', 'id'=>'addimages', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}

                    {!! Form::hidden('values_id',  '', array('class'=>'form-control', 'id'=>'values_id')) !!}

                    <input type="hidden" name="products_id" value="{{$result['products_id']}}">

                    <div class="modal-body">


                        <div class="form-group">
                            <label for="name" class="col-sm-2 col-md-3 control-label">
                                {{ trans('labels.Photos') }}
                                <span id="photo_num">
                                </span>
                            </label>
                            <div class="col-sm-10 col-md-9">
                                      <span class="help-block" style="font-weight:normal;font-size:11px;">We recommend adding 3 more photos (Max 12) |
                                          <a class="text-danger" style="font-size:12px;" href="javasript:viod(0)" onclick="removeAllImages()">Remove all</a></span>
                                <div tabindex="500" class="btn btn-primary btn-file">
                                    <i class="fa fa-folder-open"></i>
                                    <span class="hidden-xs">Browse …</span>
                                    <input type="file" style="opacity:0;" id="products_image" name="products_image[]" onchange="preview_image()" multiple/>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                                <div id="image_preview"></div>
                            </div>
                        </div>


                    </div>



                    <div class="modal-footer">
                        <button class="btn btn-primary">Submit</button>
                        {{--<div tabindex="500" class="btn btn-success btn-file" data-target="#importFromWebModal" data-toggle="modal">
                            <i class="fa fa-download"></i>
                            <span class="hidden-xs">Import from Web…</span>
                        </div>--}}
                    </div>



                    {!! Form::close() !!}



                </div>



            </div>



        </div>







        <div class="modal fade" id="productListModal" tabindex="-1" role="dialog" aria-labelledby="productListModalLabel">



            <div class="modal-dialog" role="document">



                <div class="modal-content">



                    <div class="modal-header">



                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>



                        <h4 class="modal-title" id="productListModalLabel"></h4>



                    </div>



                    <div class="modal-body">

                        <p><strong>{{ trans('labels.DeletingErrorMessage') }}</strong></p>

                        <ul style="padding:0" id="assciate-products">

                        </ul>

                    </div>



                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('labels.Ok') }}</button>

                    </div>



                </div>



            </div>



        </div>



        <!-- Main row -->

        <!-- /.row -->

    </section>

    <!-- /.content -->



</div>



@endsection