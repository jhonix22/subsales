<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;




use Validator;

use App;

use Lang;

use DB;

//for password encryption or hash protected

use Hash;

use App\Administrator;



//for authenitcate login data

use Auth;







//for requesting a value

use Illuminate\Http\Request;

class AdminPostagesController extends Controller
{
    //

    public function postages(){
        if(session('manufacturer_view')==0){

            print Lang::get("labels.You do not have to access this route");

        }else{

            $title = array('pageTitle' => Lang::get("labels.Postages"));

            $postages = DB::table('postages')

                ->select('*')

                ->where('manufacturers_info.languages_id', '1')->paginate(10);

            return view("admin.postages",$title)->with('manufacturers', $postages);

        }
    }


    //getManufacturer

    public function getPostages($language_id){

        $getPostages = DB::table('postages')

            ->select('*')

            ->where('languages_id', $language_id)->get();

        return($getPostages);

    }

}
